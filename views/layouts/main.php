<?php


use dosamigos\google\maps\LatLng;
use dosamigos\google\maps\overlays\InfoWindow;
use dosamigos\google\maps\overlays\Marker;
use dosamigos\google\maps\Map;
/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-default navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'Beranda', 'url' => ['/site/index'],],
            ['label' => 'Profil', 
            'items' => [
                ['label' => 'Sejarah', 'url'=> ['site/sejarah'],],
                ['label' => 'Visi dan Misi', 'url'=> ['site/visi-misi'],],
                ['label' => 'Logo', 'url' => ['site/logo'],],
                ['label' => 'Identitas Lengkap', 'url' => ['site/identitas'],],
            ],],
            ['label' => 'Kegiatan Siswa', 
            'items' => [
                ['label' => 'OSIS', 'url'=> ['site/osis'],],
                ['label' => 'Ekstrakurikuler', 'url'=> ['site/ekstrakurikuler'],],
            ],],
            ['label' => 'Pengumuman', 
            'items' => [
                ['label' => 'Berita', 'url'=> ['site/berita'],],
                ['label' => 'Acara', 'url'=> ['site/acara'],],
            ],],
            ['label' => 'Kontak', 'url' => ['/site/contact']],
            Yii::$app->user->isGuest ? (
                ['label' => 'Login', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container panel panel-body">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="body-content panel panel-default">
        <div class="panel panel-body">
        <div class="col-lg-4">
            <h3><strong>INFORMASI KONTAK</strong></h3><hr>
            <h4><strong>SMK BINA SISWA 2</strong></h4>
            <p>KP. Dengkeng RT 01 RW 03 <br>
            Kelurahan Rancapanggung <br>
            Kecamatan Cililin <br>
            Kabupaten Bandung Barat <br>
            Provinsi Jawa Barat <br>
            Indonesia <br>
            Kode POS 40562 </p>
            <hr>
            Telp. (022)6941626 <br>
            smkbinasiswa2@gmail.com <br>
        </div>    
        <div class="col-lg-8">
            <div class="pull-left">
                <?php 
                    echo '<h3>Lokasi SMK BINA SISWA 2</h3>';
                    echo '<div class="panel panel-default">';
                    $coord = new LatLng(['lat' => -6.9746482, 'lng' => 107.4280866]);
                    $map = new Map([
                        'center' => $coord,
                        'zoom' => 14,
                    ]);
                    
                    // Lets add a marker now
                    $marker = new Marker([
                        'position' => $coord,
                        'title' => 'SMK BINA SISWA 2',
                    ]);
                    
                    // Provide a shared InfoWindow to the marker
                    $marker->attachInfoWindow(
                        new InfoWindow([
                            'content' => '<p>SMK BINA SISWA 2</p>'
                        ])
                    );
                    
                    $map->addOverlay($marker);
                    
                    // Display the map -finally :)
                    echo $map->display();
                    echo '</div>';
                ?>
                
            </div>  
        </div>
        
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
