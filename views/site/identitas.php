<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Identitas Lengkap';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-visimisi">
    <div id="visi" class="panel panel-default">
        <div class="panel panel-body">
            <div class="col-lg-10">
                <table cellspacing="pixels">
                    <tr><h1><strong>Identitas Sekolah</strong></h1><tr>
                    <tr><td><strong>Nama Sekolah</strong></td><td>:</td><td>SMKS BINA SISWA 2</td></tr>
                    <tr><td><strong>NPSN</strong></td><td>:</td><td>20279922</td></tr>
                    <tr><td><strong>Jenjang Pendidikan</strong></td><td>:</td><td>SMK</td></tr>
                    <tr><td><strong>Status Sekolah</strong></td><td>:</td><td>Swasta</td></tr>
                    <tr><td><strong>Alamat Sekolah</strong></tr>
                </table>
            </div>
            <div class="col-lg-2">
            <center>
                <img src="images/pramuka1.jpg" alt="140x140" class="img-rounded" style="max-height:150px;"><br>
            </center>
            </div>
        </div>
    </div>
</div>
