<?php
/* @var $this yii\web\View */
use yii\helpers\Html;


$this->title = 'Beranda';
?>

    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
        </ol>
        <!-- Wrapper for slides -->
        <div class="carousel-inner img" role="listbox">
            <div class="item active" style="background-image: url(@web/images/guru1.jpg);">
                <?php echo Html::img('@web/images/guru1.jpg', ['class' => 'carousel-inner img']); ?>
                <div class="carousel-caption">
                    <h1> Judul </h1>

                    <p class="lead"> Caption</p>
                    <p><a class="btn btn-lg btn-success" href="index.php?r=site%2Findex"> link</a></p>
                </div>
            </div>
            <div class="item" style="background-image: url(@web/images/pramuka1.jpg);">>
                <?php echo Html::img('@web/images/pramuka1.jpg', ['class' => 'carousel-inner img']); ?>
                <div class="carousel-caption">
                    <h1> Judul </h1>
                    <p class="lead"> Caption</p>
                    <p><a class="btn btn-lg btn-success" href="index.php?r=site%2Findex">link</a></p>
                </div>
            </div>    
            <div class="item" style="background-image: url(@web/images/SMK.jpg);">>
                <?php echo Html::img('@web/images/SMK.jpg', ['class' => 'carousel-inner img']); ?>
                <div class="carousel-caption">
                    <h1> Judul </h1>
                    <p class="lead"> Caption</p>
                    <p><a class="btn btn-lg btn-success" href="index.php?r=site%2Findex"> link</a></p>
                </div>
            </div>
            <!-- Controls -->
            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>    
    </div>
    <div class="site-index">
    <h1><strong> BERITA TERBARU </strong></h1>
    <div class="body-content ">
        <div class="row">
            <div class="col-lg-4 panel panel-default">
                <h2>Berita</h2>

                <p>Isi berita Isi beritaIsi beritaIsi beritaIsi beritaIsi beritaIsi beritaIsi beritaIsi beritaIsi berita
                Isi beritaIsi beritaIsi beritaIsi beritaIsi beritaIsi beritaIsi beritaIsi beritaIsi beritaIsi berita
                Isi beritaIsi beritaIsi beritaIsi beritaIsi beritaIsi beritaIsi beritaIsi berita
                Isi beritaIsi beritaIsi beritaIsi beritaIsi beritaIsi beritaIsi beritaIsi berita
                Isi beritaIsi beritaIsi beritaIsi beritaIsi beritaIsi beritaIsi beritaIsi berita.</p>

                <p><a class="btn btn-default" href="index.php?r=site%2Findex">Selengkapnya &raquo;</a></p>
            </div>
            <div class="col-lg-4 panel panel-default">
                <h2>Berita</h2>

                <p>Isi berita Isi beritaIsi beritaIsi beritaIsi beritaIsi beritaIsi beritaIsi beritaIsi beritaIsi berita
                Isi beritaIsi beritaIsi beritaIsi beritaIsi beritaIsi beritaIsi beritaIsi beritaIsi beritaIsi berita
                Isi beritaIsi beritaIsi beritaIsi beritaIsi beritaIsi beritaIsi beritaIsi berita
                Isi beritaIsi beritaIsi beritaIsi beritaIsi beritaIsi beritaIsi beritaIsi berita
                Isi beritaIsi beritaIsi beritaIsi beritaIsi beritaIsi beritaIsi beritaIsi berita.</p>

                <p><a class="btn btn-default" href="index.php?r=site%2Findex">Selengkapnya &raquo;</a></p>
            </div>
            <div class="col-lg-4 panel panel-default">
                <h2>Berita</h2>

                <p>Isi berita Isi beritaIsi beritaIsi beritaIsi beritaIsi beritaIsi beritaIsi beritaIsi beritaIsi berita
                Isi beritaIsi beritaIsi beritaIsi beritaIsi beritaIsi beritaIsi beritaIsi beritaIsi beritaIsi berita
                Isi beritaIsi beritaIsi beritaIsi beritaIsi beritaIsi beritaIsi beritaIsi berita
                Isi beritaIsi beritaIsi beritaIsi beritaIsi beritaIsi beritaIsi beritaIsi berita
                Isi beritaIsi beritaIsi beritaIsi beritaIsi beritaIsi beritaIsi beritaIsi berita.</p>

                <p><a class="btn btn-default" href="index.php?r=site%2Findex">Selengkapnya &raquo;</a></p>
            </div>
        </div>
    </div>

    <div class="body-content panel panel-body-blue text-center white-text">
        <br><h1><strong>GALERI</strong> </h1><br>
        <div class="row"> 
            <div class="col-lg-4">
            <img src="images/pramuka1.jpg" alt="140x140" class="img-rounded" style="max-height:200px;"><br>
            <span class="glyphicon glyphicon-picture"> </span>
                <h4>Kegiatan kepramukaan</h4>
            </div>
            <div class="col-lg-4">
            <img src="images/keagamaan1.jpg" alt="140x140" class="img-rounded" style="max-height:200px;"><br>
            <span class="glyphicon glyphicon-picture"> </span>
                <h4>Kegiatan keagamaan (pembacaan asmaul husna)
                </h4>
            </div>
            <div class="col-lg-4">
            <img src="images/lomba-marathon1.jpg" alt="140x140" class="img-rounded" style="max-height:200px;"><br>
            <span class="glyphicon glyphicon-picture"> </span>
                <h4>Kegiatan lomba marathon
                </h4>
            </div>

            
        </div>
        <br>
        <div class="row">
            <p><a class="btn btn-default" href="index.php?r=site%2Findex">Lihat Lainnya &raquo;</a></p></div> <br>
        
    </div>
</div>
